สร้างฐานข้อมูล
CREATE DATABASE android;

สร้างตาราง products
CREATE TABLE products(
pid int(11) primary key auto_increment,
name varchar(100) not null,
price decimal(10,2) not null,
description text);


เพิ่มข้อมูลลงในตาราง products
INSERT INTO products(name,price,description) VALUES ('Tablet', 100, 'Tablet');
INSERT INTO products(name,price,description) VALUES ('Computer', 200, 'Computer');
INSERT INTO products(name,price,description) VALUES ('คอมพิวเตอร์', 200, 'คอมพิวเตอร์');
